provider "aws" {
  region                   = "eu-west-3"
  profile                  = "sops"
}

resource "aws_key_pair" "deploy" {
  key_name   = "demo-sops"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDL/SIlQHGjIsjsVygFo7IFIm+Bk4ZzBUMO+2MdxVc0rY5aPbO8wm9T/lV7EGtAGElEQidKC0T5RA4M1pyFZp+0Wn7XPeLJGi+8NUMIc/TT7EpTR8fmSSLqEZR8mFqowp//gaRqj4uTuoDJff2A9sB5/h6GloZKiZJpuFxwM0SkzqwYPMkTVHQjLgUGigYRptqNqDziQSk5QlBe212Z+RlwTenIIDxIbBA8Q98WJZm3x6OOAODt4WLT9MlZCOABkF/bOJIcYXJ7t3H+zbeJf/4Txo6kJKmzBrGob4pnuEVZJMi0V0YORiDf8diELXUs/3NM8Ji9xN1kX5E/lPvM9GCIEIo8XCvrCK0k+0dGoUnz4g6nLC4tvf5fNXLlebdGzmdILApfnTvwRBeQim865ATK3WQBwYc8PkdXHpjpzFMA/qaNxZuiuJ8nZys/tn88oaznHZiS3lXy+ksr+T1k0H7QGJgWPBAr2JYQwDFZj30b3Ybg0TcrTz7n5FgtyvbZVEs= alexis@alexis-xps-ippon"
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow_ssh"
  description = "Allow ssh inbound traffic"
  vpc_id      = "vpc-984050f1"

  ingress {
    description = "TLS from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound traffic"
  vpc_id      = "vpc-984050f1"

  ingress {
    description = "TLS from VPC"
    from_port   = 80
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "allow_http"
  }
}

resource "aws_iam_role" "sops_role" {
  name = "demo-sops_role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
  
}

resource "aws_iam_instance_profile" "sops_profile" {
  name = "demo-sops_profile"
  role = "${aws_iam_role.sops_role.name}"
}

resource "aws_iam_role_policy" "sops_policy" {
  name = "sops_policy"
  role = "${aws_iam_role.sops_role.id}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_instance" "web_srv" {
  ami = "ami-07eda9385feb1e969"
  instance_type = "t2.small"
  key_name = "demo-sops"
  security_groups = ["allow_http","allow_ssh"]
  iam_instance_profile = "${aws_iam_instance_profile.sops_profile.name}"
  tags = { Name = "web_srv"}
  depends_on = [
    aws_key_pair.deploy,
    aws_security_group.allow_http,
    aws_iam_instance_profile.sops_profile,
  ]
}


resource "aws_kms_key" "sops_key" {
  description             = "Key used to encrypt files with SOPS"
}

output "instance_ip" {
  value = aws_instance.web_srv.public_dns
} 

output "arn" {
  value = aws_kms_key.sops_key
}
